/*
CommonLibrary mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurmunlimited.mods.CommonLibrary;

import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;

import java.util.Random;
import java.util.logging.Logger;

public class CommonLibrary implements WurmServerMod {
    public static final Random random = new Random();
    public static final Logger logger = Logger.getLogger(CommonLibrary.class.getName());
    public static final String version = "0.1.1";
    @Override
    public String getVersion() {
        return version;
    }
}