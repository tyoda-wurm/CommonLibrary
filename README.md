# CommonLibrary
This is a library mod for my mods. If one of them says you should download it, then do.

# Add to gradle

To include CommonLibrary in your gradle project, add it to the repositories in build.gradle

```gradle
repositories {
    // other repositories
    maven { url "https://gitlab.com/api/v4/groups/83701471/-/packages/maven" }
}
```

and then include it as a dependency as

```gradle
dependencies {
    // other dependencies
    compileOnly 'org.tyoda.wurmunlimited.mods.CommonLibrary:CommonLibrary:<VERSION>'
}
```

or

```gradle
dependencies {
    // other dependencies
    implementation 'org.tyoda.wurmunlimited.mods.CommonLibrary:CommonLibrary:<VERSION>'
}
```

replacing `<VERSION>` with the current latest release version.

# Add to maven

To include CommonLibrary in your maven project, add it to the repositories in pom.xml


```xml
<repository>
    <id>commonlibrary-gitlab</id>
    <name>CommonLibrary GitLab repository</name>
    <url>https://gitlab.com/api/v4/groups/83701471/-/packages/maven</url>
</repository>
```

Then add the dependency

```xml
<dependency>
  <groupId>org.tyoda.wurmunlimited.mods.CommonLibrary</groupId>
  <artifactId>CommonLibrary</artifactId>
  <version><VERSION></version>
</dependency>
```

replacing `<VERSION>` with the current latest release version.

(full disclosure: I don't know anything about maven, hopefully nobody tries to use this)
